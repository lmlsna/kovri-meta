# IRC log to HTML parser

This is an unsafe prototype of a python script that will wrap the user,
content, line, and emotes all in their own span classes and in an unordered
list. I haven't done any sanatizing of output yet so just a proof of concept
for turning IRC meetings into servable HTML files that we can integrate with
kovri.io as it's own section, if that is desirable.

## Usage
`./logs-to-html raw-log.txt [output-file.html]`



