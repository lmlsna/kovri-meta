#!/usr/bin/env python3

# Wildly unsafe irc log parser v0.0

import os
import sys
import re

spaces = '    '
tab = '	'


# How to process logs
head = '<!doctype html><html><head><title>Log</title><style>.chat-user{ color: blue;} .chat-emote { color: purple; font-weight: bold; } .chat-text { color: black; } ul { list-style-type: none; list-style: none;}</style></head><body>'
foot = '</body></html>'

prepend_all = '<ul class="chat-log">'
append_all = '</ul>'

prepend_line = '<li class="chat-line">'
append_line = '</li>'

prepend_user = '<span class="chat-user">'
append_user = '</span><span class="sep">: </span>'

prepend_emote = '<span class="chat-emote">'
append_emote = '</span>'

prepend_text = '<span class="chat-text">'
append_text = '</span>'


# Make sure we at least have an input file argument
if len(sys.argv) > 1 :
	input_file = sys.argv[1]
else :
	sys.stderr.write('Usage: {} plain_text.log [output_text.md]'.format(os.path.basename(sys.argv[0])))
	sys.exit(1)

# Use second arg as output if given, otherwise use input file with .md extension
if len(sys.argv) > 2 :
	output_file = sys.argv[2]
else :
	output_file = input_file.replace( os.path.splitext(input_file)[0], 'html')

# Make sure input file really exists
if not os.path.exists(input_file) :
	sys.stderr.write('Plain text log file could not be found at "{}"'.format(input_file))
	sys.exit(2)
# And that the output file doesn't so no accidental overwriting
elif os.path.exists(output_file) :
	sys.stderr.write('Output file "{}" already exists, cautiously aborting...'.format(output_file))
	sys.exit(3)

# Read lines into array from file
fn = open(input_file)
lines = fn.readlines()
fn.close()
new = [head, prepend_all]

# If first line is special, use as title
if not lines[0].find(spaces) :
	title = '{}'.format(lines[0])
# Otherwise use filename
else :
	title = 'Log File: {}'.format(output_file)
new.append('{}'.format(title))
lines.remove(lines[0])

# Flip stack, pop and process
lines.reverse()
while len(lines) > 0 :
	line = lines.pop()
	if re.match('^\*    ',line) :
		new.append('{}{}{}'.format(prepend_emote, line, append_emote))
	else :
		(user,text) = line.split(spaces)
		user = '{}{}{}'.format(prepend_user, user, append_user)
		text = '{}{}{}'.format(prepend_text, text, append_text)
		newline = '{}{}'.format(user, text)
	new.append('{}{}{}'.format(prepend_line, newline, append_line))

new.append(append_all)
new.append(foot)
out = '\n'.join(new)

print(out)
